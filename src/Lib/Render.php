<?php

namespace Nikolajev\RenderParams\Lib;

use Nikolajev\Filesystem\Search;

class Render
{
    private function prepareRenderingData(string $filePath)
    {
        $composerPath = (new Search())->findFileFromParentDirs($filePath, 'composer.json');

        // @todo File::json()
        $composerData = json_decode(file_get_contents($composerPath), true);

        $namespace = array_keys($composerData['autoload']['psr-4'])[0];
        $namespaceDir = array_values($composerData['autoload']['psr-4'])[0];

        $namespaceDirRelativePath = explode($namespaceDir, $filePath)[1];

        $namespaceSuffix = dirname($namespaceDirRelativePath);
        $namespaceSuffix = $namespaceSuffix === '.' ?
            null :
            str_replace(DIRECTORY_SEPARATOR, "\\", $namespaceSuffix);


        $fullRenderedFileNamespace = substr($namespace, 0, -1) . $namespaceSuffix;

        if (str_ends_with($fullRenderedFileNamespace, "\\")) {
            $fullRenderedFileNamespace = substr($fullRenderedFileNamespace, 0, -1);
        }

        $renderedClassTitle = explode(".", basename($filePath))[0] . "Params";
        $renderedFileTitle = "$renderedClassTitle.php";

        $renderedFilePath = dirname($filePath) . DIRECTORY_SEPARATOR . $renderedFileTitle;

        return [$renderedFilePath, $renderedClassTitle, $fullRenderedFileNamespace, $this->parseParams($filePath)];
    }

    private function parseParams($filePath)
    {
        $result = [];

        $contents = file_get_contents($filePath);

        foreach (explode(PHP_EOL, $contents) as $line) {
            $result[] = $this->paramStringToArgv($line);
        }

        return $result;
    }

    // @url https://stackoverflow.com/questions/34868421/get-argv-from-a-string-with-php
    private function paramStringToArgv(string $str): ?array
    {
        // Get rid of: sh: 1: array: not found
        $str = str_replace("|", "\|", $str);

        $serializedArguments = shell_exec(
            sprintf('php -r "array_shift(\\$argv); echo serialize(\\$argv);" -- %s', $str)
        );

        $result = unserialize($serializedArguments);

        return ($result === false) ? [] : $result;
    }

    public function render(string $filePath)
    {
        list($renderedFilePath, $renderedClassTitle, $fullRenderedFileNamespace, $params) =
            $this->prepareRenderingData($filePath);

        $paramSectionTemplate = file_get_contents(dirname(__DIR__, 2) . '/templates/paramSection.sprintf.txt');

        $paramsString = "";

        foreach ($params as $paramData) {
            if (empty($paramData)) {
                continue;
            }
            list($title, $type, $value) = $paramData;

            if (
                str_contains($type, 'string') ||
                (empty($value) && !is_numeric($value))
            ) {
                if ($value !== '[]') {
                    $value = "'$value'";
                }
            }

            $paramsString .= sprintf($paramSectionTemplate, $type, $title, $value, $title, $type, $title, $type);
        }

        $fileTemplate = file_get_contents(dirname(__DIR__, 2) . '/templates/file.sprintf.txt');

        $renderedContents = sprintf($fileTemplate, $fullRenderedFileNamespace, $renderedClassTitle, $paramsString);

        $action = !file_exists($renderedFilePath) ? 'created' : 'updated';

        if (
            $action === 'created' ||
            ($action === 'updated' && $renderedContents !== file_get_contents($renderedFilePath))
        ) {
            success("$action: $renderedFilePath");
        }

        file_put_contents($renderedFilePath, $renderedContents);
    }
}