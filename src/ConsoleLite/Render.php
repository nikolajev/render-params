<?php

namespace Nikolajev\RenderParams\ConsoleLite;

use Nikolajev\ConsoleLite\ConsoleCommand;
use Nikolajev\Filesystem\FilesList;
use Nikolajev\Filesystem\FilesListParams;

class Render extends ConsoleCommand
{
    public function exec()
    {
        // @todo Make them static
        $filesList = FilesList::list(
            '/var/www/repositories',
            (new FilesListParams)
                ->excludedDirPatterns(['*/.git', '*/vendor'])
                ->includedFilenamePatterns(['*.params.render'])
        );


        foreach ($filesList as $filePath) {
            (new \Nikolajev\RenderParams\Lib\Render())->render($filePath);
        }
    }
}